import jwt
import time
from datetime import datetime, timedelta
import paho.mqtt.client as mqtt
 
SSL_PRIV_KEY_PATH = "/home/pi/Desktop/source/iot_gcp/src/dht22_private.pem"
ROOT_CERT_PATH = "/home/pi/Desktop/source/iot_gcp/src/roots.pem"
PROJECT_ID = "iot-monitoring-298106"
GCP_LOCATION = "asia-east1"
REGISTRY_ID = "pi_iot_monitoring"
DEVICE_ID = "pi-iot-device"
 
_CLIENT_ID = f"projects/{PROJECT_ID}/locations/{GCP_LOCATION}/registries/{REGISTRY_ID}/devices/{DEVICE_ID}"
_MQTT_TOPIC = f"/devices/{DEVICE_ID}/events"
 
 
class IoTManager:
    def __init__(self):
        self.connected = False
        self.client = mqtt.Client(client_id=_CLIENT_ID)
        self.client.on_connect = self.on_connect
        self.client.on_disconnect = self.on_disconnect
        self.client.on_publish = self.on_publish

        #root_cert_file = open(ROOT_CERT_PATH, 'rb').read()

        self.client.tls_set(ROOT_CERT_PATH)
        self.last_temp = -999
        self.last_humidity = -1
        self.last_lux = 0
        self.last_gas = 0
        self.last_gas_ppm = 0.0
        self.exp = 0
 
    def create_jwt(self):
        now = datetime.utcnow()
        self.exp = now + timedelta(minutes=60)
        token = {
            'iat': now,
            'exp': self.exp,
            'aud': PROJECT_ID
        }
        with open(SSL_PRIV_KEY_PATH, 'r') as f:
            private_key = f.read()
 
        self.client.username_pw_set(
            username='unused',
            password=jwt.encode(token, private_key, "RS256")
        )
 
    def print_result(self, event, res) -> str:
        print(f"{event}: {mqtt.error_string(res) if isinstance(res, int) else res}")
 
    def on_connect(self, unusued_client, unused_userdata, unused_flags, rc):
        self.print_result("on_connect", rc)
        self.connected = True
 
    def on_disconnect(self, unusued_client, unused_userdata, unused_flags, rc):
        self.print_result("on_disconnect", rc)
        self.connected = False
 
    def on_publish(self, unused_client, unused_userdata, unused_mid):
        self.print_result("on_publish", None)
 
    def on_message(self, client, userdata, message):
        self.print_result("on_message", message)
 
    def connect(self):
        self.create_jwt()
        self.client.connect('mqtt.googleapis.com', 8883)
        self.client.loop_start()
 
    def disconnect(self):
        print("disconnecting IoT")
        self.client.disconnect()
 
    def publish_data(self, temp, humidity, lux, gas, gas_ppm) -> bool:
        # skip publishing to IoT if temp/humidity didn't change since the last publish
        if self.last_temp == temp and self.last_humidity == humidity and self.last_lux == lux and self.last_gas == gas and self.last_gas_ppm == gas_ppm:
            return True
        self.last_temp = temp
        self.last_humidity = humidity
        self.last_lux = lux
        self.last_gas = gas
        self.last_gas_ppm = gas_ppm

        if not self.connected or self.exp < datetime.utcnow():
            self.connect()

        #kst_localtime = time.mktime(time.localtime())
        payload = f"{{ 'ts': {int(time.time())}, 'temperature': {temp}, 'humidity': {humidity}, 'lux': {lux}, 'gas': {gas}, 'gas_ppm': {gas_ppm} }}"
        print(payload)
        try:
            rc = self.client.publish(_MQTT_TOPIC, payload, qos=1)
            rc.wait_for_publish()
            print("Published into GCP IoT")
            return True
        except ValueError as e:
            print(e)
        return False
