import os
import flask
from flask import request
from flask import Response
import time
import logging
import json
import datetime
import werkzeug
import optparse
import tornado.wsgi
import tornado.httpserver
import Adafruit_DHT as dht
import smbus
import RPi.GPIO as GPIO
import busio
import digitalio
import board
import adafruit_mcp3xxx.mcp3008 as MCP
from adafruit_mcp3xxx.analog_in import AnalogIn
from mq135 import MQ135

### for MQ135-Sensor CO2 calculation

### for MQ135-Sensor CO2 calculation

# for lux, i2c
I2C_CH = 1
BH1750_DEV_ADDR = 0x23

# lux mode
CONT_H_RES_MODE     = 0x10
CONT_H_RES_MODE2    = 0x11
CONT_L_RES_MODE     = 0x13
ONETIME_H_RES_MODE  = 0x20
ONETIME_H_RES_MODE2 = 0x21
ONETIME_L_RES_MODE  = 0x23

# for MQ-135 sensor
# Create the SPI bus
spi = busio.SPI(clock=board.SCK, MISO=board.MISO, MOSI=board.MOSI)

# Create the cs (chip select)
cs = digitalio.DigitalInOut(board.D5)

# Create the mcp object
mcp = MCP.MCP3008(spi, cs)

# Create analog inputs connected to the input pins on the MCP3008.
channel_0 = AnalogIn(mcp, MCP.P0)

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)

app = flask.Flask(__name__)

#  Define application commands and features:
def _range(x, in_min, in_max, out_min, out_max):
    return int((x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min)

def http_error_response(error_msg, status_code):
    data = {
        'status_code': status_code,
        'msg': error_msg
    }

    js = json.dumps(data)

    res = Response(js, status=status_code, mimetype='application/json')
    logger.error(error_msg)
    return res
	
def http_success_response(message, result):
    data = {
        'status_code': 200,
        'msg' : message,
        'result' : result
    }

    js = json.dumps(data)

    res = Response(js, status=200, mimetype='application/json')
    return res

@app.route('/iot',  methods=['GET'])
def iot():
    now = datetime.datetime.now()
    strtime = now.strftime('%Y-%m-%d %H:%M:%S')

    # temperature, humidity
    hum, temp = dht.read_retry(dht.DHT22,4)
   
    #temp = 21
    #hum = 25

    # lux sensor
    i2c = smbus.SMBus(I2C_CH)
    luxBytes = i2c.read_i2c_block_data(BH1750_DEV_ADDR, CONT_H_RES_MODE, 2)
    lux = int.from_bytes(luxBytes, byteorder='big')

    # MQ-135 gas sonsor
    # Test your module, then define the value range - in this case between 0 and 60000.
    #print("channel_o.value={:0.2f}".format(channel_0.value))
    #map((value_ads - 565), 0, 26690, 0, 1023)
    gas = _range(channel_0.value, 0, 60000, 0, 1023)
    gas_status_string = "Green"
    
    #print("channel_0.value={},temp={},hum={}".format(channel_0.value), temp, hum)
    mq135_calc = MQ135()
    mq135_calc.set_value(gas)
    #co2_correctedPPM = mq135_calc.get_corrected_ppm(temp, hum)
    #co2_correctedPPM = 10
    #co2_correctedPPM = getCorrectedPPM(temp,hum,CORA,CORB,CORC,CORD,CORE,CORF,CORG,gas,RLOAD,PARA,RZERO,PARB)

    if gas > 300:
        gas_status_string = "Red"

    rzero = mq135_calc.get_rzero()
    corrected_rzero = mq135_calc.get_corrected_rzero(temp, hum)
    resistance = mq135_calc.get_resistance()
    ppm = mq135_calc.get_ppm()
    corrected_ppm = mq135_calc.get_corrected_ppm(temp, hum)

    #print_temp_hum = "{} Temperature={:0.2f}, humidity={:0.2f}, lux={}, gas={}, gas_status={}, RZero={:0.2f}, Corrected RZero={:0.2f}, Resistance={:0.2f}, ppm={:0.2f}, Corrected ppm={:0.2f}".format(strtime, temp, hum, lux, gas, gas_status_string, rzero, corrected_rzero, resistance, ppm, corrected_ppm)

    json_result = {
        "data": {
            "temperature": temp,
            "humidity": hum,
            "lux": lux,
            "gas": gas,
            "gas_status": gas_status_string,
            "rzero": rzero,
            "corrected_rzero": corrected_rzero,
            "resistance": resistance,
            "ppm": ppm,
            "corrected_ppm": corrected_ppm
        }
    }

    return http_success_response("success", json_result)



def start_tornado(app, port=5000):
    http_server = tornado.httpserver.HTTPServer(
        tornado.wsgi.WSGIContainer(app))
    http_server.listen(port)
    logging.info("Tornado server starting on port {}".format(port))
    tornado.ioloop.IOLoop.instance().start()
  

if __name__ == '__main__':
    start_tornado(app, 5000)
 






 

