import os
import time
import logging
import json
import datetime
import Adafruit_DHT as dht
import smbus
import RPi.GPIO as GPIO
import busio
import digitalio
import board
import adafruit_mcp3xxx.mcp3008 as MCP
from adafruit_mcp3xxx.analog_in import AnalogIn
from mq135 import MQ135
from app_gcp_iot_manager import IoTManager
from time import sleep

### for MQ135-Sensor CO2 calculation
# for lux, i2c
I2C_CH = 1
BH1750_DEV_ADDR = 0x23

# lux mode
CONT_H_RES_MODE     = 0x10
CONT_H_RES_MODE2    = 0x11
CONT_L_RES_MODE     = 0x13
ONETIME_H_RES_MODE  = 0x20
ONETIME_H_RES_MODE2 = 0x21
ONETIME_L_RES_MODE  = 0x23

# Upload data interval to GCP 
READING_INTERVAL_SEC = 900  # 15 mins

#  Define application commands and features:
def _range(x, in_min, in_max, out_min, out_max):
    return int((x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min)

def main():
    try:
        iot = IoTManager()

        # for MQ-135 sensor
        # Create the SPI bus
        spi = busio.SPI(clock=board.SCK, MISO=board.MISO, MOSI=board.MOSI)

        # Create the cs (chip select)
        cs = digitalio.DigitalInOut(board.D5)

        # Create the mcp object
        mcp = MCP.MCP3008(spi, cs)

        # Create analog inputs connected to the input pins on the MCP3008.
        channel_0 = AnalogIn(mcp, MCP.P0)
 
        while True:
            # humidity, temperature sensor
            hum, temp = dht.read_retry(dht.DHT22,4)

            # lux sensor
            i2c = smbus.SMBus(I2C_CH)
            luxBytes = i2c.read_i2c_block_data(BH1750_DEV_ADDR, CONT_H_RES_MODE, 2)
            lux = int.from_bytes(luxBytes, byteorder='big')
 
            gas = _range(channel_0.value, 0, 60000, 0, 1023)

            #print("channel_0.value={},temp={},hum={}".format(channel_0.value), temp, hum)
            mq135_calc = MQ135()
            mq135_calc.set_value(gas)

            rzero = mq135_calc.get_rzero()
            corrected_rzero = mq135_calc.get_corrected_rzero(temp, hum)
            resistance = mq135_calc.get_resistance()
            ppm = mq135_calc.get_ppm()
            corrected_ppm = mq135_calc.get_corrected_ppm(temp, hum)
            gas_ppm = corrected_ppm

            res = iot.publish_data(f"{temp:0.2f}", f"{hum:0.2f}", f"{lux}", f"{gas}", f"{gas_ppm:0.2f}")
            print(f"Temperature={temp:0.2f}, humidity={hum:0.2f}, lux={lux}, gas={gas}, gas_ppm={gas_ppm:0.2f}")

            if res == True:
                print("iot online..")
            else:
                print("iot offline..")
            
            time.sleep(READING_INTERVAL_SEC)
 
    except KeyboardInterrupt:
        pass
 
    # cleanup
    iot.disconnect()

if __name__ == '__main__':
    main()
 






 

