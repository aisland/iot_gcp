FROM python:3.7.2-stretch

RUN mkdir /webapp
ADD entrypoint.sh /webapp
ADD ./src /webapp

WORKDIR /webapp

RUN pip install --upgrade pip && pip install -r requirements.txt

ENTRYPOINT ["/bin/bash", "entrypoint.sh"]
